# Hi there, I'm Niklas, also known as Taucher2003 👋

### About me:

I am from Germany and currently <!--timespan:start(%y)(env:1)-->19<!--timespan:end--> years old. I'm very interested in programming and so did I start 
with basic Minecraft Java Plugins in 2019 and now I also make Discord Bots and other stuff!

### Connect with me:

[<img src="https://img.shields.io/badge/Taucher2003%231578-7289DA.svg?&style=for-the-badge&logo=discord&logoColor=white"/>][taucherdiscord] 
[<img src="https://img.shields.io/badge/Taucher2003-181717.svg?&style=for-the-badge&logo=github&logoColor=white"/>][github]
[<img src="https://img.shields.io/badge/Taucher2003-6E49CB.svg?&style=for-the-badge&logo=gitlab&logoColor=white"/>][gitlab]
<br>
<br>
<br>

### Languages & Tools

<img src="https://img.shields.io/badge/java-FFFFFF.svg?&style=for-the-badge&logo=openjdk&logoColor=black"/> <img src="https://img.shields.io/badge/c%23%20-239120.svg?&style=for-the-badge&logo=c-sharp&logoColor=white"/> 
<img src="https://img.shields.io/badge/html5%20-E34F26.svg?&style=for-the-badge&logo=html5&logoColor=white"/> <img src="https://img.shields.io/badge/css3%20-1572B6.svg?&style=for-the-badge&logo=css3&logoColor=white"/> <img src="https://img.shields.io/badge/javascript%20-F7DF1E.svg?&style=for-the-badge&logo=javascript&logoColor=grey"/> <img src="https://img.shields.io/badge/node.js-339933.svg?&style=for-the-badge&logo=nodedotjs&logoColor=white"/> <!--img src="https://img.shields.io/badge/typescript%20-3178C6.svg?&style=for-the-badge&logo=typescript&logoColor=white"/--> <!--img src="https://img.shields.io/badge/php-777BB4.svg?&style=for-the-badge&logo=php&logoColor=white"/--> <img src="https://img.shields.io/badge/bash-4EAA25.svg?&style=for-the-badge&logo=gnu%20bash&logoColor=white"/> <img src="https://img.shields.io/badge/ruby-CC342D.svg?&style=for-the-badge&logo=ruby&logoColor=white"/>

<img src="https://img.shields.io/badge/spring-6DB33F.svg?&style=for-the-badge&logo=spring&logoColor=white"/> <img src="https://img.shields.io/badge/spring%20boot-6DB33F.svg?&style=for-the-badge&logo=springboot&logoColor=white"/> <img src="https://img.shields.io/badge/graphql-E10098.svg?&style=for-the-badge&logo=graphql&logoColor=white"/> <img src="https://img.shields.io/badge/vue-4FC08D.svg?&style=for-the-badge&logo=vue.js&logoColor=white"/> <img src="https://img.shields.io/badge/vuetify-1867C0.svg?&style=for-the-badge&logo=vuetify&logoColor=white"/> <img src="https://img.shields.io/badge/webpack-8DD6F9.svg?&style=for-the-badge&logo=webpack&logoColor=white"/> <!--img src="https://img.shields.io/badge/node.js%20-339933.svg?&style=for-the-badge&logo=node.js&logoColor=white"/--> <img src="https://img.shields.io/badge/postresql-4169E1.svg?&style=for-the-badge&logo=postgresql&logoColor=white"/> <img src="https://img.shields.io/badge/mariadb-003545.svg?&style=for-the-badge&logo=mariadb&logoColor=white"/> <img src="https://img.shields.io/badge/redis-DC382D.svg?&style=for-the-badge&logo=redis&logoColor=white"/> <!--img src="https://img.shields.io/badge/mongodb%20-47A248.svg?&style=for-the-badge&logo=mongodb&logoColor=white"/--> <img src="https://img.shields.io/badge/docker-2496ED.svg?&style=for-the-badge&logo=docker&logoColor=white"/> <img src="https://img.shields.io/badge/kubernetes-326CE5.svg?&style=for-the-badge&logo=kubernetes&logoColor=white"/> <img src="https://img.shields.io/badge/terraform-7B42BC.svg?&style=for-the-badge&logo=terraform&logoColor=white"/>

<img src="https://img.shields.io/badge/-IntelliJ%20IDEA-5464c8?style=for-the-badge&logo=intellij%20idea&logoColor=white"/> <!--img src="https://img.shields.io/badge/eclipse-2C2255.svg?&style=for-the-badge&logo=eclipse&logoColor=white"/--> <!--img src="https://img.shields.io/badge/visual%20studio-5C2D91.svg?&style=for-the-badge&logo=visual%20studio&logoColor=white"/--> <!--img src="https://img.shields.io/badge/rider-faaa14.svg?&style=for-the-badge&logo=rider&logoColor=white"/--> <img src="https://img.shields.io/badge/visual%20studio%20code-007ACC.svg?&style=for-the-badge&logo=visual%20studio%20code&logoColor=white"/> <!--img src="https://img.shields.io/badge/atom-0aa372.svg?&style=for-the-badge&logo=atom&logoColor=white"/--> <img src="https://img.shields.io/badge/git-F05032.svg?&style=for-the-badge&logo=git&logoColor=white"/> <img src="https://img.shields.io/badge/github%20-181717.svg?&style=for-the-badge&logo=github&logoColor=white"/> <img src="https://img.shields.io/badge/gitlab%20-6E49CB.svg?&style=for-the-badge&logo=gitlab&logoColor=white"/> <img src="https://img.shields.io/badge/insomnia%20-5849BE.svg?&style=for-the-badge&logo=insomnia&logoColor=white"/> <img src="https://img.shields.io/badge/maven-C71A36.svg?&style=for-the-badge&logo=apache%20maven&logoColor=white"/> <img src="https://img.shields.io/badge/gradle-02303A.svg?&style=for-the-badge&logo=gradle&logoColor=white"/> <img src="https://img.shields.io/badge/prometheus-E6522C.svg?&style=for-the-badge&logo=prometheus&logoColor=white"/> <img src="https://img.shields.io/badge/grafana-F46800.svg?&style=for-the-badge&logo=grafana&logoColor=white"/>
<br>
<br>
<br>

### Certifications & Badges

[<img src="https://raw.githubusercontent.com/Taucher2003/Taucher2003/master/assets/GitLab-Certified-Associate.png" height="200px">][gitlab-certified-associate]
[<img src="https://raw.githubusercontent.com/Taucher2003/Taucher2003/master/assets/gitlab_mvp_badge.png" height="200px">][gitlab-mvp-15-7]
<br>
<br>

[![@taucher2003's Holopin board](https://holopin.me/taucher2003)](https://holopin.io/@taucher2003)

### Projects that I am part of:
| Project | Position | Timespan |
|---------|----------|----------|
| ~~GΛMΞFM TeamSpeak & Webradio~~ <br>*(Project discontinued)* | ~~Manager & Developer of the Discordbot~~ | 16.06.2018 - 30.08.2020 |
| Vrox Network | Founder, Manager & Leading Developer | since 01.07.2018 |
| Traidio Network | Administrator | since 17.04.2019 |
| LOST ROCKET Development <p>● ReactionRole Discordbot | Developer & Support Manager<p> Leading Developer | since 30.01.2020 <p> since 30.01.2020 |
| ~~Hamibot~~ <br>*(Team left)* | ~~Developer~~ | 22.09.2020 - 07.03.2021 |
| NotABot | System Administrator & DevOps Engineer | since 07.03.2021 |
| Reputation Bot | Collaborator | since 30.06.2021 |
<!--| MarniX Developments | Founder, CTO | since 03.08.2020 |-->

<p>
 
[![Discord](https://img.shields.io/discord/758702426248970270?color=62e7f7&label=Vrox%20Network&logo=discord&style=flat-square)][vroxdiscord]
[![Discord](https://img.shields.io/discord/485875390976622593?color=fafafa&label=Traidio%20Network&logo=discord&style=flat-square)][traidiodiscord]
[![Discord](https://img.shields.io/discord/289819432992243712?color=99beff&label=LOST%20ROCKET%20Development&logo=discord&style=flat-square)][lostrocketdiscord]
[![Discord](https://img.shields.io/discord/803679267303981056?color=c32047&label=NotABot&logo=discord&style=flat-square)][notabotdiscord]
[![Discord](https://img.shields.io/discord/853250161915985958?color=fdb846&label=Reputation%20Bot&logo=discord&style=flat-square)][reputationbotdiscord]
 
<!-- DEPRECATED SHIELDS -->
<!--![Discord](https://img.shields.io/discord/717002750499618918?color=de4190&label=MarniX%20Developments&logo=discord&style=flat-square)-->
<!--[![Discord](https://img.shields.io/discord/423385448295956491?color=191529&label=G%CE%9BM%CE%9EFM&logo=discord&style=flat-square)][gamefmdiscord]-->
<!--[![Discord](https://img.shields.io/discord/715988026479607891?color=1d67dd&label=Hamibot&logo=discord&style=flat-square)][hamibotdiscord]-->
 

---
<br>

 ⚡ **Recent GitLab Activity**

<!--RECENT_ACTIVITY:start-->
👥 Joined [Henrik Steffens / mensa-discord](https://gitlab.com/Th3Ph4nt0m/mensa-discord) \
📬 Opened Merge Request [!44](https://gitlab.com/taucher2003-group/t2003-utils/-/merge_requests/44) in [Taucher2003-Group / T2003-Utils](https://gitlab.com/taucher2003-group/t2003-utils) \
🗨️ Commented on Issue [#388048](https://gitlab.com/gitlab-org/gitlab/-/issues/388048#note_1247846584) in [GitLab.org / GitLab](https://gitlab.com/gitlab-org/gitlab) \
🗨️ Commented on Issue [#388840](https://gitlab.com/gitlab-org/gitlab/-/issues/388840#note_1247840321) in [GitLab.org / GitLab](https://gitlab.com/gitlab-org/gitlab) \
🗨️ Commented on Merge Request [!1904](https://gitlab.com/gitlab-org/quality/triage-ops/-/merge_requests/1904#note_1247455291) in [GitLab.org / Quality Department / triage-ops](https://gitlab.com/gitlab-org/quality/triage-ops) \
🗨️ Commented 2 times on Merge Request [!1889](https://gitlab.com/gitlab-org/quality/triage-ops/-/merge_requests/1889#note_1247357880) in [GitLab.org / Quality Department / triage-ops](https://gitlab.com/gitlab-org/quality/triage-ops) \
🗨️ Commented on Merge Request [!109389](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/109389#note_1247156063) in [GitLab.org / GitLab](https://gitlab.com/gitlab-org/gitlab) \
🗨️ Commented 2 times on Merge Request [!109377](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/109377#note_1247145971) in [GitLab.org / GitLab](https://gitlab.com/gitlab-org/gitlab) \
🗨️ Commented on Merge Request [!1889](https://gitlab.com/gitlab-org/quality/triage-ops/-/merge_requests/1889#note_1247126972) in [GitLab.org / Quality Department / triage-ops](https://gitlab.com/gitlab-org/quality/triage-ops) \
🗨️ Commented on Merge Request [!1904](https://gitlab.com/gitlab-org/quality/triage-ops/-/merge_requests/1904#note_1247112671) in [GitLab.org / Quality Department / triage-ops](https://gitlab.com/gitlab-org/quality/triage-ops)
<!--RECENT_ACTIVITY:end-->

 ⚡ **My Week**

<!--START_SECTION:waka-->

```text
Java             10 hrs 46 mins  ████████████████▒░░░░░░░░   64.83 %
YAML             2 hrs 52 mins   ████▒░░░░░░░░░░░░░░░░░░░░   17.29 %
Kotlin           51 mins         █▒░░░░░░░░░░░░░░░░░░░░░░░   05.17 %
XML              39 mins         █░░░░░░░░░░░░░░░░░░░░░░░░   03.95 %
CLASS            31 mins         ▓░░░░░░░░░░░░░░░░░░░░░░░░   03.19 %
Ruby             17 mins         ▒░░░░░░░░░░░░░░░░░░░░░░░░   01.74 %
```

<!--END_SECTION:waka-->



  
  <details>
 <summary>⚡ WakaTime Stats (All Time, since 24 Sep 2020)</summary>
  <img src="https://wakatime.com/share/@30a41e50-568b-4814-8487-1688250ab14e/53d96858-f9a4-473d-ab90-fe937a18d346.svg" width="600px">
  <img src="https://wakatime.com/share/@30a41e50-568b-4814-8487-1688250ab14e/514c7cf9-b341-4ea4-9f1a-c70ba3b801f1.svg" width="600px">
  <img src="https://wakatime.com/share/@30a41e50-568b-4814-8487-1688250ab14e/14d75efe-ef68-40b3-ad2a-92e36e55fdfd.svg" width="600px">
 </details>
 <details>
 <summary>⚡ WakaTime Stats (Last 30 Days)</summary>
 <img src="https://wakatime.com/share/@30a41e50-568b-4814-8487-1688250ab14e/2bc449fd-1ebc-4fdd-84ef-cc46318983ef.svg" width="600px">
 <img src="https://wakatime.com/share/@30a41e50-568b-4814-8487-1688250ab14e/12ab2c12-b456-4ee9-a5bd-2f167c3d3da1.svg" width="600px">
 <img src="https://wakatime.com/share/@30a41e50-568b-4814-8487-1688250ab14e/f11db079-5513-400a-8733-dd69132e3070.svg" width="600px">
 </details>
 


[taucherdiscord]: https://discord.com/users/444889694002741249
[gitlab]: https://gitlab.com/Taucher2003
[github]: https://github.com/Taucher2003
[vroxdiscord]: https://discord.gg/rCj7MeU
[traidiodiscord]: https://discord.gg/xjFkW8a
[gamefmdiscord]: https://discord.gg/QfG3kPM
[lostrocketdiscord]: https://discord.gg/UPM7KkB
[reactionroleinvite]: https://discord.com/oauth2/authorize?client_id=664849019654111233&permissions=268790848&scope=bot
[hamibotdiscord]: https://discord.gg/7QGMbuC
[notabotdiscord]: https://discord.gg/CSCYeNfA77
[reputationbotdiscord]: https://discord.gg/wrqrUJGuru

[gitlab-certified-associate]: https://gitlab.edcast.com/pathways/cy-test-pathway-associate-study-exam
[gitlab-mvp-15-7]: https://about.gitlab.com/releases/2022/12/22/gitlab-15-7-released/#mvp
[gitlab-heroes]: https://about.gitlab.com/community/heroes/
